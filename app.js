// Теоретичні питання:
// 1. Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// AJAX - це технологія, яка дозволяє обмінюватися даними між браузером і сервером без перезавантаження самої веб сторінки. Корисна дана технологія при авторизації, відправки даних форм, додавання коментарів, відправки повідомлень.

// Завдання:
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.
// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// const url = "https://ajax.test-danit.com/api/swapi/films";

const getMovies = () => {
  return fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(response => response.json())
    // .then(data => console.log(data))
    .then(data => data)
    .catch(error => console.log(error));
};

// getMovies();

const getCharacters = () => {
  getMovies().then(data =>
    data.forEach(movie => {
      showMovieInfo(movie);
      movie.characters.forEach(character => {
        fetch(character)
          .then(response => response.json())
          .then(data => {
            let listOfChar = document.querySelector(`.listOfChar-${movie.episodeId}`);
            listOfChar.innerHTML += `<div>${data.name}</div>`;
          });
      });
    })
  );
};

const showMovieInfo = (movie) => {
  const movieInfo = document.querySelector('#root');

  movieInfo.insertAdjacentHTML(
    "beforeend",
    `<div>
    <h2 style="text-transform: uppercase">${movie.episodeId}. ${movie.name}</h2>
    <p>${movie.openingCrawl}</p>
    <div class="listOfChar-${movie.episodeId}"></div>
    </div>`
  );
}

getCharacters();